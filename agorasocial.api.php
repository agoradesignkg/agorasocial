<?php

/**
 * @file
 * Hooks provided by the agorasocial module.
 */

/**
 * Alter the result of agorasocial_get_available_social_networks().
 *
 * @param string[] &$available_social_networks
 *   An array containing the available social networks, keyed by its ID and
 *   having the labels as values.
 */
function hook_agorasocial_available_networks_alter(array &$available_social_networks) {
  $available_social_networks['pinterest'] = 'Pinterest';
}

/**
 * Alter the result of agorasocial_fontawesome_mapping().
 *
 * @param string[] &$icon_mapping
 *   An array containing the social network ID as keys and the corresponding
 *   FontAwesome icon name (without "fa-" prefix) as values.
 */
function hook_agorasocial_fontawesome_mapping_alter(array &$icon_mapping) {
  if (isset($icon_mapping['facebook'])) {
    $icon_mapping['facebook'] = 'facebook-f';
  }
}
