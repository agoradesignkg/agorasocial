<?php

namespace Drupal\agorasocial;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a value object for representing social media links.
 */
final class SocialLink {

  /**
   * The type.
   *
   * @var string
   */
  protected string $type;

  /**
   * The name.
   *
   * @var string
   */
  protected string $name;

  /**
   * The url.
   *
   * @var string
   */
  protected string $url;

  /**
   * Constructs a new SocialLink object.
   *
   * @param string $type
   *   The type.
   * @param string $name
   *   The name.
   * @param string $url
   *   The url.
   */
  public function __construct(string $type, string $name, string $url) {
    $this->type = $type;
    $this->name = $name;
    $this->url = $url;
  }

  /**
   * Get the type.
   *
   * @return string
   *   The type.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Get the name.
   *
   * @return string
   *   The name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Get the url.
   *
   * @return string
   *   The url.
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * Get an array representation of this object.
   *
   * @return array
   *   An array representation of the social media link.
   */
  public function toArray(): array {
    return [
      'type' => $this->type,
      'name' => $this->name,
      'url' => $this->url,
    ];
  }

  /**
   * Get an Url object.
   *
   * @return \Drupal\Core\Url
   *   The Drupal Url object.
   */
  public function toUrl(): Url {
    return Url::fromUri($this->url);
  }

  /**
   * Get a Link object.
   *
   * @return \Drupal\Core\Link
   *   The Drupal Link object.
   */
  public function toLink(): Link {
    return Link::fromTextAndUrl($this->name, $this->toUrl());
  }

}
