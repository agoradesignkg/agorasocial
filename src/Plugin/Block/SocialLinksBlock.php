<?php

namespace Drupal\agorasocial\Plugin\Block;

use Drupal\agorasocial\SocialLinkRendererInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the social links block.
 *
 * @Block(
 *   id = "social_links",
 *   admin_label = @Translation("Social links"),
 *   category = "agoradesign"
 * )
 */
class SocialLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The social link renderer.
   *
   * @var \Drupal\agorasocial\SocialLinkRendererInterface
   */
  protected $socialLinkRenderer;

  /**
   * Constructs a new SocialLinksBlock object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\agorasocial\SocialLinkRendererInterface $social_link_renderer
   *   The social link renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SocialLinkRendererInterface $social_link_renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->socialLinkRenderer = $social_link_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('agorasocial.social_link_renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->socialLinkRenderer->renderSocialLinks();
  }

}
