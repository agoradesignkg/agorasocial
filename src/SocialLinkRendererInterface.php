<?php

namespace Drupal\agorasocial;

/**
 * Defines the social link renderer interface.
 */
interface SocialLinkRendererInterface {

  /**
   * Renders the social links.
   *
   * @return array
   *   A render array.
   */
  public function renderSocialLinks(): array;

  /**
   * Loads the social links.
   *
   * @return \Drupal\agorasocial\SocialLink[]
   *   The social links.
   */
  public function loadSocialLinks(): array;

}
