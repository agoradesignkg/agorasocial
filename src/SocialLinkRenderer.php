<?php

namespace Drupal\agorasocial;

use Drupal\site_settings\SiteSettingsLoaderInterface;
use Drupal\site_settings\SiteSettingsLoaderPluginManager;

/**
 * Default social link renderer implementation.
 */
class SocialLinkRenderer implements SocialLinkRendererInterface {

  /**
   * The site settings loader.
   *
   * @var \Drupal\site_settings\SiteSettingsLoaderInterface
   */
  protected SiteSettingsLoaderInterface $siteSettingsLoader;

  /**
   * Static cache of available social media links.
   *
   * @var \Drupal\agorasocial\SocialLink[]
   */
  protected array $socialLinks;

  /**
   * Constructs a new SocialLinksBlock object.
   *
   * @param \Drupal\site_settings\SiteSettingsLoaderPluginManager $site_settings_loader_manager
   *   The site settings loader plugin manager.
   */
  public function __construct(SiteSettingsLoaderPluginManager $site_settings_loader_manager) {
    $this->siteSettingsLoader = $site_settings_loader_manager->getActiveLoaderPlugin();
    $this->socialLinks = [];
  }

  /**
   * {@inheritdoc}
   */
  public function renderSocialLinks(): array {
    $output = [
      'social_media' => [
        '#cache' => [
          'tags' => ['site_setting_entity_list'],
        ],
      ],
    ];

    $social_links = $this->loadSocialLinks();
    foreach ($social_links as $social_link) {
      $output['social_media'][] = [
        '#theme' => 'social_network',
        '#type' => $social_link->getType(),
        '#name' => $social_link->getName(),
        '#url' => $social_link->getUrl(),
      ];
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function loadSocialLinks(): array {
    if (empty($this->socialLinks)) {
      $available_networks = agorasocial_get_available_social_networks();
      $social_media_site_settings = $this->siteSettingsLoader->loadByGroup('social_media');
      if ($social_media_site_settings && !empty($social_media_site_settings['social_media'])) {
        foreach ($social_media_site_settings['social_media'] as $social_network) {
          if (!empty($social_network['field_social_network']) && !empty($social_network['field_url'])) {
            $type = $social_network['field_social_network'];
            $name = $available_networks[$type] ?? $type;
            $url = $social_network['field_url']['uri'];
            $this->socialLinks[$type] = new SocialLink($type, $name, $url);
          }
        }
      }
    }
    return $this->socialLinks;
  }
}
